(function(KoreSDK){

    var KoreSDK=KoreSDK||{};
    var chatConfig=window.KoreSDK.chatConfig; 

    chatConfig.i18n={
        rtlLanguages:['ar','es'],
        availableLanguages:['en','ar','es'],//shown as list of available languages in chat window header to select
        defaultLanguage:"en",//default selection from above list
        languageStrings:{   //any additional language can be added in this object by adding the key in availableLanguages
            ar: {
                message: "رسالة...",
                connecting: "توصيل ...",
                reconnecting: "جاري إعادة الاتصال ...",
                entertosend: "اضغط على Enter للإرسال",
                endofchat: "نهاية سجل الدردشة",
                loadinghistory: "تحميل محفوظات الدردشة ..",
                sendText:"إرسال",
                closeText:"قريب",
                expandText:"وسعت",
                minimizeText:"تصغير",
                reconnectText:"أعد الاتصال",
                attachmentText:"المرفق"
            },
            es:{
                message:"Mensaje...",
                connecting:"Conectando...",
                reconnecting:"Reconectando...",
                entertosend:"Presiona Enter para enviar",
                endofchat:"Fin del historial de chat",
                loadinghistory:"Descargar historial de conversaciones..",
                sendText:"enviar",
                closeText:"cerrar",
                expandText:"buscado",
                minimizeText:"disminuir el zoom",
                reconnectText:"Recordar",
                attachmentText:"Adjunto"
            }
        }
    }

    KoreSDK.chatConfig=chatConfig

})(window.KoreSDK);
