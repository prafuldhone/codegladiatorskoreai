$(document).ready(function() {
    $("#chatContainer").hide();
    $('.openChatWindow').hide();
    /* $(".button-submit").click(function() {
      $("#loginContainer").hide(100);
      $("#chatContainer").show(100);
    }); */

    $('#first_form').submit(function(e) {
      e.preventDefault();
     /*  var username = $('#username').val();
      var last_name = $('#last_name').val(); */
      var email = $('#email').val();
      var password = $('#password').val();
  
      $(".error").remove();
  
      /* if (username.length < 1) {
        $('#username').after('<span class="error">This field is required</span>');
      }
      if (last_name.length < 1) {
        $('#last_name').after('<span class="error">This field is required</span>');
      } */
      
      if (email.length < 1) {
        $('#email').after('<span class="error">This field is required</span>');
        return true;
      } else {
        var regEx = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        var validEmail = regEx.test(email);
        var validUser1 = email === isUser1;
        var validUser2 = email === isUser2;
      var validUser3 = email === isUser3;
      var validUser4 = email === isUser4;
      var validUser5 = email === isUser5;
		
        var validPassword = password === isPassword;
        console.log(!validEmail && validUser1);
        console.log(!validEmail && validUser2);
        if (validEmail && (validUser1 || validUser2 || validUser3 || validUser4 || validUser5)) {
          console.log('valid email and valid user');
          // set userEmail local storage
          localStorage.setItem("userEmail", email);
          // set interactive Language and phone number
          if(email === 'prafuldhone@gmail.com') {
            localStorage.setItem("userName", 'Praful');
            localStorage.setItem("userPhoneNumber", '9730196416');
            localStorage.setItem("interactiveLanguage", 'en');
            setTimeout(function() {
              KORE_CUSTOM_DATA['userName'] = localStorage.getItem("userName");
              KORE_CUSTOM_DATA['interactiveLanguage'] = localStorage.getItem("interactiveLanguage");              
            }, 0);
            document.body.style.backgroundImage = "url('./img/background_english.jpg')";

          } else if(email === 'atleefdes@givmail.com') {
            localStorage.setItem("userName", 'Atlee');
            localStorage.setItem("userPhoneNumber", '9730196416');
            localStorage.setItem("interactiveLanguage", 'de');
            setTimeout(function() {
              KORE_CUSTOM_DATA['userName'] = localStorage.getItem("userName");
              KORE_CUSTOM_DATA['interactiveLanguage'] = localStorage.getItem("interactiveLanguage");
            }, 0);
            document.body.style.backgroundImage = "url('./img/background_german.jpg')";
          } else if(email === 'harshalpatil007@gmail.com') {
            localStorage.setItem("userName", 'Harshal');
            localStorage.setItem("userPhoneNumber", '9730196416');
            localStorage.setItem("interactiveLanguage", 'en');
            setTimeout(function() {
              KORE_CUSTOM_DATA['userName'] = localStorage.getItem("userName");
              KORE_CUSTOM_DATA['interactiveLanguage'] = localStorage.getItem("interactiveLanguage");
            }, 0);
            document.body.style.backgroundImage = "url('./img/background_english.jpg')";
          } else {
            localStorage.setItem("userName", 'Rohit');
            localStorage.setItem("userPhoneNumber", '9889876632');
            localStorage.setItem("interactiveLanguage", 'de');
            setTimeout(function() {
              KORE_CUSTOM_DATA['userName'] = localStorage.getItem("userName");
              KORE_CUSTOM_DATA['interactiveLanguage'] = localStorage.getItem("interactiveLanguage");
            }, 0);
            document.body.style.backgroundImage = "url('./img/background_german.jpg')";
          }
          // load this two file for updated username after login
          $.getScript("kore-config.js");
          $.getScript("kore-main.js");          
        } else {
          $('#email').after('<span class="error">Enter a valid email</span>');
          return true;
        }
      }
      const passwordLenth = password.length < 8;
      console.log(typeof validPassword)
      if (!validPassword && !passwordLenth) {
        //$('#password').after('<span class="error">Password must be at least 8 characters long</span>');
        $('#password').after('<span class="error">Enter valid password.</span>');
        return true;
      }
      //window.location.reload();
      $("#loginContainer").hide(100);
      $.getScript("kore-config.js");
      $("#chatContainer").show(100);     

    });

  });


  