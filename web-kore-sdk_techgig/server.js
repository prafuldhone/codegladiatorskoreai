// Variables
var path = require('path');
var express = require('express');
var app = require('express')();
var http = require('http').Server(app);

const tokenRoute = require('./Routes/tokenRoute');


const bodyParser = require('body-parser');
const cors = require('cors');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(cors());

app.use((err, req, res, next) => {
    return res.send({
      "statusCode": "300",
      "statusMessage": "Something went wrong"
    });
  });

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.use('/token', tokenRoute);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next();
});

//Configure port
var port=8082;

//App directories
var PROJECT_DIR = path.normalize(__dirname);

app.use('/',express.static(path.join(PROJECT_DIR, '')));

http.listen(port, function(){
    console.log('Sample Application runnning at http://localhost:'+port+'/UI');
});
