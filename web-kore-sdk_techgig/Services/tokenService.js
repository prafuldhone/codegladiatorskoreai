
var jwt = require('jsonwebtoken');

getToken = async function (req, res) {

    try {
        var identity = req.body.identity;
        var clientId = req.body.clientId;
        var clientSecret = req.body.clientSecret;
        var isAnonymous = req.body.isAnonymous || false;
        var aud = req.body.aud || "https://idproxy.kore.com/authorize";

        var options = {
            "iat": new Date().getTime(),
            "exp": new Date(new Date().getTime() + 24 * 60 * 60 * 1000).getTime(),
            "aud": aud,
            "iss": clientId,
            "sub": identity,
            "isAnonymous": isAnonymous
        }
        var token = jwt.sign(options, clientSecret);
		console.log("token : ", token)
        res.send({ "jwt": token });
    } catch (error) {
        console.log("error", error.message)
        res.status(404).json({ message: 'something went wrong', error, error })
    }
}

module.exports = {
    getToken: getToken
}

