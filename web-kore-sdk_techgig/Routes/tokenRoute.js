const express = require("express");
const router = express.Router({ mergeParams: true });
const tokenService = require("../Services/tokenService");

/* User Registration. */
router.post("/getToken", tokenService.getToken);


module.exports = router;